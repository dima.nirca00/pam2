package com.example.mynewapp;

import androidx.appcompat.app.AppCompatActivity;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.SearchManager;
import android.content.Intent;

import android.os.Build;
import android.os.Bundle;

import android.provider.MediaStore;

import android.widget.Button;
import android.widget.EditText;




public class MainActivity extends AppCompatActivity {

    private static final String CHANNEL_ID = "channel_id01";
    private static final int NOTIFICATION_ID = 1;

    EditText editText;
    Button buttonSearch;
    Button BackCamera;
    Button FrontCamera;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button showNotificationBtn = findViewById(R.id.showNotificationBtn);
        showNotificationBtn.setOnClickListener(v -> {

            showNotification();
        });

        editText = findViewById(R.id.editText);
        buttonSearch = findViewById(R.id.btnSearch);
        buttonSearch.setOnClickListener(v -> {
                Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
                String input = editText.getText().toString();
                intent.putExtra(SearchManager.QUERY, input);
                startActivity(intent);
        });

        BackCamera = (Button) findViewById(R.id.BackCamera);
        BackCamera.setOnClickListener(v -> {
            Intent Intent1 = new Intent(MediaStore.INTENT_ACTION_STILL_IMAGE_CAMERA);
            startActivity(Intent1);
        });

        FrontCamera = (Button) findViewById(R.id.FrontCamera);
        FrontCamera.setOnClickListener(v -> {
            Intent Intent2 = new Intent(MediaStore.INTENT_ACTION_STILL_IMAGE_CAMERA);
            startActivity(Intent2);
        });

    }

    private void showNotification() {

        createNotificationChannel();

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);

        builder.setSmallIcon(R.drawable.ic_notification);

        builder.setContentTitle("Hello! I'm your notification!");

        builder.setContentText("Congrats!! You've made it!");

        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);



        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
        notificationManagerCompat.notify(NOTIFICATION_ID, builder.build());
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){

            CharSequence name = "My Notification";
            String description = "My notification description";

            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationChannel.setDescription(description);

            NotificationManager notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

            notificationManager.createNotificationChannel(notificationChannel);
        }
    }



}